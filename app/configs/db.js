const mongoose = require('mongoose');

require('../models/UserModel');

exports.init_db = () => {
  mongoose.set('useCreateIndex', true);
  mongoose.Promise = global.Promise;

  process.env.MONGO_URI = `mongodb://localhost/${process.env.MONGO_DB_NAME}`;
  if (process.env.NODE_ENV === 'test') {
    process.env.MONGO_URI = `mongodb://mongo/${process.env.MONGO_DB_TEST_NAME}`;
  }
  mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};