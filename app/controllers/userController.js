const mongoose = require('mongoose');
const validator = require('email-validator');

const User = mongoose.model('Users');

module.exports = {
  stockPushKey: (req, res) => {
    if (!req.body.key) {
      return res.status(400).json({
        code: 'E_MISSING_FIELD',
        message: 'Field key missing',
        status: 400,
        data: null,
      });
    }
    const c = JSON.stringify(req.body.key);
    User.findByIdAndUpdate(req.user._id, {
      pushKey: c,
    }, (err, currentUser) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      if (!currentUser) {
        return res.status(400).json({
          code: 'E_USER_ID_ERROR',
          message: err,
          status: 400,
          data: null,
        });
      }
      return res.json({
        code: 'S_KEY_PUSH',
        message: 'User push key added',
        status: 200,
        data: {
          name: currentUser.name,
          email: currentUser.email,
          follows: currentUser.follows,
          role: currentUser.role,
        },
      });
    });
  },

  getUserProfile: (req, res) => {
    User.findById(req.user._id, (err, currentUser) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      if (!currentUser) {
        return res.status(400).json({
          code: 'E_USER_ID_ERROR',
          message: err,
          status: 400,
          data: null,
        });
      }
      return res.json({
        code: 'S_PROFILE',
        message: 'User information',
        status: 200,
        data: {
          name: currentUser.name,
          email: currentUser.email,
          follows: currentUser.follows,
          role: currentUser.role,
        },
      });
    });
  },

  getUsers: (req, res) => {
    User.find({}, (err, list) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      list.forEach((elem) => {
        elem.password = '';
        return true;
      });
      return res.json({
        code: 'S_LIST_USERS',
        message: 'Users list',
        status: 200,
        data: list,
      });
    });
  },

  createUser: (req, res) => {
    if (!req.body.name || !req.body.email || !req.body.password) {
      return res.status(400).json({
        code: 'E_MISSING_FIELD',
        message: 'Field name or email or password missing',
        status: 400,
        data: null,
      });
    }
    if (!validator.validate(req.body.email)) {
      return res.status(400).json({
        code: 'E_EMAIL_FORMAT_ERROR',
        message: 'Wrong email format',
        status: 400,
        data: null,
      });
    }
    User.findOne({
      email: req.body.email,
    }, (err, user) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      if (user) {
        return res.status(400).json({
          code: 'E_USER_ALREADY_EXIST',
          message: 'User with this email already exist',
          status: 400,
          data: null,
        });
      }

      const newUser = User(req.body);
      newUser.save((error, x) => {
        if (error) {
          return res.status(500).json({
            code: 'E_MONGODB_ERROR',
            message: error,
            status: 500,
            data: null,
          });
        }

        return res.json({
          code: 'S_REGISTERED',
          message: 'User registered',
          status: 200,
          data: {
            id: x._id,
            name: x.name,
            email: x.email,
          },
        });
      });
      return null;
    });
    return null;
  },

  updateUser: (req, res) => {
    if (!req.params.email) {
      return res.status(400).json({
        code: 'E_MISSING_FIELD',
        message: 'Field email missing',
        status: 400,
        data: null,
      });
    }
    if (req.body.email && !validator.validate(req.body.email)) {
      return res.status(400).json({
        code: 'E_EMAIL_FORMAT_ERROR',
        message: 'Wrong email format',
        status: 400,
        data: null,
      });
    }
    if (req.body.password) delete req.body.password;
    User.findOneAndUpdate({
        email: req.params.email,
      },
      req.body, {
        new: true,
      },
      (err, x) => {
        if (err) {
          return res.status(500).json({
            code: 'E_MONGODB_ERROR',
            message: err,
            status: 500,
            data: null,
          });
        }
        return res.json({
          code: 'S_UPDATED',
          message: `User ${x.email} updated`,
          status: 200,
          data: {
            id: x._id,
            name: x.name,
            email: x.email,
          },
        });
      });
    return null;
  },

  deleteUser: (req, res) => {
    if (!req.params.email) {
      return res.status(400).json({
        code: 'E_MISSING_FIELD',
        message: 'Field email',
        status: 400,
        data: null,
      });
    }
    User.findOneAndDelete({
      email: req.params.email,
    }, (err) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      return res.json({
        code: 'S_DELETED',
        message: `User ${req.params.email} deleted`,
        status: 200,
        data: null,
      });
    });
  },

};