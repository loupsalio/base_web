const passport = require('passport');
const express = require('express');
const controller = require('../controllers/userController');

const router = express.Router();

/**
 * Get logged user informations
 * @route GET /users/me
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.get(
  '/me',
  passport.authenticate('jwt', {
    session: false,
  }),
  controller.getUserProfile,
);

/**
 * Get all users informations
 * @route GET /users
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.get(
  '/',
  passport.authenticate('admin', {
    session: false,
  }),
  controller.getUsers,
);

/**
 * Create a new user
 * @route POST /users
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.post(
  '/',
  passport.authenticate('admin', {
    session: false,
  }),
  controller.createUser,
);

/**
 * Update user informations
 * @route PATCH /users/:mail
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.patch(
  '/:email',
  passport.authenticate('admin', {
    session: false,
  }),
  controller.updateUser,
);

/**
 * Delete a user
 * @route PATCH /users/:mail
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.delete(
  '/:email',
  passport.authenticate('admin', {
    session: false,
  }),
  controller.deleteUser,
);

module.exports = router;