require('dotenv-safe').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');

const app = express();

// Setup express server port from ENV, default: 3000
app.set('port', process.env.PORT || 3000);

// Enable only in development HTTP request logger middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// for parsing json
app.use(
  bodyParser.json({
    limit: '20mb',
  }),
);

// for parsing application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: '20mb',
    extended: true,
  }),
);

// MongoDB configuration
require('./app/configs/db').init_db();

// Init all other stuff
app.use(cors());
app.use(helmet());

// Init passport
require('./app/configs/passport.js');

// Init routes
app.use('/', require('./app/routes/main'));
app.use('/auth', require('./app/routes/auth'));
app.use(
  '/users',
  // passport.authenticate("jwt", { session: false }),
  require('./app/routes/user'),
);

// 404 error
app.all('*', (req, res) => res.status(404).json({
  code: 'E_NOT_FOUND',
  message: `${req.originalUrl} path not found`,
  status: 404,
  data: null,
}));

// Server listen on port

app.listen(
  app.get('port'),
  console.log(`[${process.env.NAME}] App listening on port : ${process.env.PORT}`),
);

// For testing

module.exports = app;