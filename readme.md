# base_web

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
Docker / Docker-compose
```

or

```
NodeJS / npm
```

### Installing

First check you installed the requisites, then clone the repository and install the dependencies :

```bash
// Docker

git clone https://gitlab.com/loupsalio/base_web.git
cd base_web
docker-compose up --build
```

or

```bash
// npm

git clone https://gitlab.com/loupsalio/base_web.git
cd base_web
npm i
npm start (nodemon : npm run dev)
```

## Routes

Port 3000

## Running the tests

All tests are in the tests/\*.js file

If you want to add some tests, just check the basic fonctions as example

```bash
npm test
```

## Authors

- **Lucas CLEMENCEAU** - _Developer_ - [Loupsalio](https://gitlab.com/loupsalio)
